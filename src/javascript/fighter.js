class Fighter {
	constructor(fighter) {
		this.name = fighter.name;
		this.health = fighter.health;
		this.attack = fighter.attack; 
		this.defense = fighter.defense;
	}

	getHitPower() {
		let criticalHitChance = Math.random() + 1;
		let power = Math.round(this.attack * criticalHitChance);
		return power;
	}

	getBlockPower() {
		let dodgeChance = Math.random() + 1;
		let power = Math.round(this.defense * dodgeChance);
		return power;
	}
}

export default Fighter;