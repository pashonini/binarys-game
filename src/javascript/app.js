import FightersView from './fightersView';
import FighterSelect from './fighterSelect';
import Fight from './fightPage';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static next = document.getElementById('nextState');
  static loadingElement = document.getElementById('loading-overlay');
  static state;
  static fighters;
  static configPage;
  static selectPage;
  static fightPage;

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      App.next.addEventListener('click', this.clickHandler, false);
      
      App.fighters = await fighterService.getFighters();
      App.configPage = new FightersView(App.fighters);
      
      App.rootElement.appendChild(App.configPage.element);
      App.state = "config";
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

  clickHandler() {
    App.rootElement.removeChild(App.rootElement.firstElementChild);

    switch (App.state) {
      case 'config': 
        App.selectPage = new FighterSelect(App.fighters, App.configPage.fightersDetailsMap);
        console.log(App.selectPage.element);
        App.rootElement.appendChild(App.selectPage.element);
        App.state = "select";
        break;
      case 'select': 
        App.fightPage = new Fight(...App.selectPage.players);
        App.rootElement.appendChild(App.fightPage.element);
        App.state = "fight";
        break;
      case 'fight': 
        App.rootElement.appendChild(App.configPage.element);
        App.state = "config";
        break;
      default: 
        console.log('state unknown');
    }
  }
}

export default App;