import View from './view';

class Fight extends View {
	constructor(player1, player2) {
		super();

		this.createFight(player1, player2);
		this.fight(player1, player2);
	}

	createFight(player1, player2) {
		let bar = pid => {
	    	 const elem = document.createElement("td");
	    	 elem.innerHTML = `
	    	 	<div id="${pid}" class="health-bar" style="float: ${pid == "bar1" ? "right" : "left"};">
	                <div class="bar">
	                  <div class="hit"></div>
	                </div>
              	</div>
	    	 `;
	    	 return elem;
	    };

	    let act = pid => {
	    	 const elem = document.createElement("td");
	    	 elem.innerHTML = `
	    	 	<img id="${pid}" src="./resources/action/${pid == "act1" ? player1.name : player2.name}-static.png" style="${pid == "act1" ? "float: right;" : "transform: scaleX(-1); float: left;"}">
	    	 `;
	    	 return elem;
	    };

	    const logger = this.createElement({ tagName: 'td', attributes: {colspan: "2"} });
	    const p = document.createElement("p");
	    p.id = "log";
	   	logger.appendChild(p);

	    let fst = document.createElement("tr");
	    fst.append(...[bar("bar1"), bar("bar2")]);
	    let snd = document.createElement("tr");
	    snd.append(...[act("act1"), act("act2")]);
	    let trd = document.createElement("tr");
	    trd.appendChild(logger);

	    const body = document.createElement("tbody");
	    body.append(...[fst, snd, trd]);

		this.element = this.createElement({ tagName: 'table', className: 'aligner' });
	    this.element.appendChild(body);
	}

	fight(player1, player2) {
		let firstHit = Math.random() >= 0.5;
		let duration = player => {
			let value;
			switch (player.name) {
		      case 'Bison': 
		        value = 580;
		        break;
		      case 'Dhalsim': 
		        value = 480;
		        break;
		      case 'Guile': 
		        value = 520;
		        break;
		        case 'Ken': 
		        value = 640;
		        break;
		      case 'Ryu': 
		        value = 580;
		        break;
		      case 'Zangief': 
		        value = 520;
		        break;
		      default: 
		        console.log('name unknown');
		    }
		    return value;
		}
		
		player1.dur = duration(player1);
		player2.dur = duration(player2);

		player1.pos = "1";
		player2.pos = "2";
		
		player1.total = player1.health;
		player2.total = player2.health;
		
		let action = (player1, player2) => {
			let attacker = firstHit ? player1 : player2,
			defender = firstHit ? player2 : player1;

			let anim = document.getElementById(`act${attacker.pos}`);
			anim.src = `./resources/action/${attacker.name}.gif`;
			setTimeout(function(){
			  anim.src = `./resources/action/${attacker.name}-static.png`;
			}, attacker.dur);

			let damage = attacker.getHitPower() - defender.getBlockPower();
			damage = damage > 0 ? damage : 0;

			let barWidth = ( (defender.health - damage) / defender.total) * 100;
			let hitWidth = (damage / defender.health) * 100 + "%";
			defender.health -= damage;

			let bar = document.getElementById(`bar${defender.pos}`).getElementsByClassName("bar")[0];
			let hit = document.getElementById(`bar${defender.pos}`).getElementsByClassName("hit")[0];
			hit.style.width = hitWidth;

			setTimeout(function(){
			  hit.style.width = 0;
			  bar.style.width = barWidth + "%";
			}, 500);

			this.log(attacker, damage);
			
			if (defender.health <= 0) {
				this.log(`${defender.name} is DEAD, ${attacker.name} win!\nGAME OVER!`);
				clearInterval(fight);
			}

			firstHit = !firstHit;
		}
		let fight = setInterval(action, 1000, player1, player2);
	}

	log(attacker, damage){
	  let log = document.getElementById("log");
	  
	  if(damage !== undefined) {
		  log.innerText = attacker.name + " dealed " + damage + " points of damage!";
	  } else {
	    log.innerText = attacker;
	  }
	};
}

export default Fight;