import View from './view';
import Fighter from './fighter';
import { fighterService } from './services/fightersService';

class FighterSelect extends View {
	constructor(fighters, fightersMap) {
		super();

		this.fightersConfig = fightersMap;
		this.handleClick = this.handleIconClick.bind(this);
		this.handleConfirm = this.confirmHandler.bind(this);
		this.createSelect(fighters);
		this.players = [];
	}

	static selection;

	createSelect(fighters) {
		document.getElementById("nextState").disabled = true;

		const header = this.createElement({ tagName: 'td', className: 'header', attributes: {colspan: "5"} });
		header.innerText = "PLAYER SELECT";

		const fighterIcons = fighters.map(fighter => {
	      const icon = this.createElement({ tagName: 'img', className: 'icon', attributes: {src: `./resources/icons/${fighter.name}.png`, title: `${fighter.name}`} });
	      icon.addEventListener('click', event => this.handleClick(fighter), false);
	      return icon;
	    });
	    const iconContainer = this.createElement({ tagName: 'td', attributes: {colspan: "5"} });
	    iconContainer.id = "selector";
	    iconContainer.append(...fighterIcons);

	    let player = pid => {
	    	 const elem = this.createElement({ tagName: 'td', attributes: {colspan: "2"} });
	    	 elem.id = pid;
	    	 elem.innerHTML = `
	    	 	<img src="./resources/icons/none.jpg">
              	<p>Name: <span>None</span></p>
              	<p>Health: <span>0</span></p>
              	<p>Attack: <span>0</span></p>
              	<p>Defense: <span>0</span></p>
	    	 `;
	    	 return elem;
	    };

	    const sep = document.createElement("td");
	    sep.innerHTML = `
	    	<img src="./resources/versus.png" width="150"><br>
	    `;
	    const button = this.createElement({ tagName: 'button', className: 'button' });
	    button.innerText = "Confirm";
	    button.addEventListener('click',  event => this.handleConfirm(event), false);
	    sep.appendChild(button);

	    let fst = document.createElement("tr");
	    fst.appendChild(header);
	    let snd = document.createElement("tr");
	    snd.appendChild(iconContainer);
	    let trd = document.createElement("tr");
	    trd.append(...[player("player1"), sep, player("player2")]);

	    const body = document.createElement("tbody");
	    body.append(...[fst, snd, trd]);
	    
	    this.element = this.createElement({ tagName: 'table', className: 'aligner' });
	    this.element.appendChild(body);
	}

	async handleIconClick(fighter) {
		if (!this.fightersConfig.get(fighter._id)) {
	      let details = await fighterService.getFighterDetails(fighter._id);
	      this.fightersConfig.set(fighter._id, details);
    	}
    	let details = this.fightersConfig.get(fighter._id);
    	let info = document.getElementById(`${!this.players[0] ? "player1" : "player2"}`);
    	info.getElementsByTagName("img")[0].src = `./resources/icons/${details.name}.png`;
    	let desc = info.getElementsByTagName("span");
    	desc[0].innerText = `${details.name}`;
    	desc[1].innerText = `${details.health}`;
    	desc[2].innerText = `${details.attack}`;
    	desc[3].innerText = `${details.defense}`;

    	FighterSelect.selection = details;
	}

	confirmHandler(event) { 
		if (!FighterSelect.selection) return;
		this.players.push(new Fighter(FighterSelect.selection));
		if (this.players.length == 2) {
			let selector = document.getElementById("selector");
			let clone = selector.cloneNode(true);
			selector.parentNode.replaceChild(clone, selector);

			event.target.disabled = true;
			document.getElementById("nextState").disabled = false;
		}
	}
}

export default FighterSelect;