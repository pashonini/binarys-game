import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  modal = document.getElementById("details");

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);

    const close = document.getElementsByClassName("close")[0];
    close.addEventListener('click', event => this.modalClose(event), false);
    window.addEventListener('click', event => {if (event.target == this.modal) this.modalClose(event);}, false);
  }

  modalClose(event) {
    let details = this.fightersDetailsMap.get(this.modal.getAttribute("key"));
    let values = this.modal.querySelectorAll("input[type='number']");
    details.health = +values[0].value;
    details.attack = +values[1].value;
    details.defense = +values[2].value;

    this.fightersDetailsMap.set(this.modal.getAttribute("key"), details);
    this.modal.style.display = "none";
  }

  async handleFighterClick(event, fighter) {
    if (!this.fightersDetailsMap.get(fighter._id)) {
      let details = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, details);
    }

    this.modal.style.display = "block";
    this.modal.setAttribute("key", `${fighter._id}`);

    let details = this.fightersDetailsMap.get(fighter._id);

    let list = document.getElementById("insertData");
    list.innerHTML = `
      <tbody>
        <tr>
          <td class="desc">Name:</td>
          <td>${details.name}</td>
        </tr>
        <tr>
          <td class="desc">Health:</td>
          <td><input type="number" value="${details.health}"></input></td>
        </tr>
        <tr>
          <td class="desc">Attack:</td>
          <td><input type="number" value="${details.attack}"></input></td>
        </tr>
        <tr>
          <td class="desc">Defense:</td>
          <td><input type="number" value="${details.defense}"></input></td>
        </tr>
      </tbody>
    `;
  }
}

export default FightersView;